@extends("layout.app")

@section("subheader")
    @include("layout.include.subheader", [
        "pages" => [
            [
                "name" => "Mon Compte",
                "link" => route('Account.Profil.index')
            ]
        ],
        "back" => false
    ])
@endsection

@section("content")

@endsection

@section("script")

@endsection
