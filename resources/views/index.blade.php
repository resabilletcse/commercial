@extends("layout.app")

@section("subheader")
    @include("layout.include.subheader", [
        "pages" => [
            [
                "name" => "Tableau de Bord",
                "link" => route('home')
            ]
        ],
        "back" => false
    ])
@endsection

@section("content")
    <div class="alert alert-info  fade show" role="alert">
        <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
        <div class="alert-text">
            Pour acceder à la billetterie, veuillez utiliser les identifiants suivant:<br>
            <strong>Email:</strong> user0@billet.com<br>
            <strong>Mot de passe:</strong> user0
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>
    <div id="rdvPrevious"></div>
    <div class="row">
        <div class="col-md-8">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body" id="map">

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Vos rendez-vous
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body" id="loadRdv">

                </div>
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Enregistrement
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <button id="btn-start-recording" class="btn btn-icon btn-warning">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z M12,20 C7.581722,20 4,16.418278 4,12 C4,7.581722 7.581722,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 Z" fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                    </button>
                    <button id="btn-stop-recording" class="btn btn-icon btn-warning" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M8,6 L10,6 C10.5522847,6 11,6.44771525 11,7 L11,17 C11,17.5522847 10.5522847,18 10,18 L8,18 C7.44771525,18 7,17.5522847 7,17 L7,7 C7,6.44771525 7.44771525,6 8,6 Z M14,6 L16,6 C16.5522847,6 17,6.44771525 17,7 L17,17 C17,17.5522847 16.5522847,18 16,18 L14,18 C13.4477153,18 13,17.5522847 13,17 L13,7 C13,6.44771525 13.4477153,6 14,6 Z" fill="#000000"/>
                            </g>
                        </svg>
                    </button>
                    <br>
                    <div><audio controls autoplay playsinline></audio></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="//maps.google.com/maps/api/js?key=AIzaSyBJPIZ4AUHlkaOEvOZTGrrI9-qu2XgYUsM" type="text/javascript"></script>
    <script src="https://raw.githubusercontent.com/HPNeo/gmaps/master/gmaps.js" type="text/javascript"></script>
    <script src="{{ asset('js/audio.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/map.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/index.js') }}" type="text/javascript"></script>
@endsection
