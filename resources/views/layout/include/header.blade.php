<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-header__top">
        <div class="kt-container ">

            <!-- begin:: Brand -->
            <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                <div class="kt-header__brand-logo">
                    <a href="{{ route('home') }}">
                        <img alt="Logo" src="/storage/logo_carre.png" width="80" class="kt-header__brand-logo-default" />
                        <img alt="Logo" src="/storage/logo_carre.png" width="50" class="kt-header__brand-logo-sticky" />
                    </a>
                </div>
            </div>

            <!-- end:: Brand -->

            <!-- begin:: Header Topbar -->
            <div class="kt-header__topbar">

                <!--begin: Notifications -->
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
											<span class="kt-header__topbar-icon  {{ \App\Helpers\Notificator::pulsing(count(auth()->user()->unreadNotifications)) }}">

												<!--<i class="flaticon2-bell-alarm-symbol"></i>-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3" />
														<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000" />
													</g>
												</svg> <span class="kt-pulse__ring"></span>
											</span>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                        <form>

                            <!--begin: Head -->
                            <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(assets/media/misc/bg-1.jpg)">
                                <h3 class="kt-head__title">
                                    Vos notifications
                                    &nbsp;
                                    <span class="btn btn-success btn-sm btn-bold btn-font-md">{{ count(auth()->user()->unreadNotifications) }} {{ \App\Helpers\Generator::pluralizer('Notification', count(auth()->user()->unreadNotifications)) }}</span>
                                </h3>
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events" role="tab" aria-selected="false">Evenements</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Logs Systèmes</a>
                                    </li>
                                </ul>
                            </div>

                            <!--end: Head -->
                            <div class="tab-content">
                                <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                                    <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                        @foreach(auth()->user()->notifications as $notification)
                                        @if($notification->data['type'] == 'alert')
                                        <a data-href="@if(!empty($notification->data['link'])) {{ $notification->data['link'] }} @endif" class="kt-notification__item @if($notification->read_at != null) kt-notification__item--read @endif" data-id="{{ $notification->id }}">
                                            <div class="kt-notification__item-icon">
                                                <i class="{{ $notification->data['icon'] }} kt-font-{{ $notification->data['icon_color'] }}"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title">
                                                    {{ $notification->data['title'] }}
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    {{ $notification->updated_at->diffForHumans() }}
                                                </div>
                                            </div>
                                        </a>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                    <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                        @foreach(auth()->user()->notifications as $notification)
                                            @if($notification->data['type'] == 'event')
                                                <a href="#" class="kt-notification__item @if($notification->read_at != null) kt-notification__item--read @endif" data-id="{{ $notification->id }}">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="{{ $notification->data['icon'] }} kt-font-{{ $notification->data['icon_color'] }}"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title">
                                                            {{ $notification->data['title'] }}
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            {{ $notification->updated_at->diffForHumans() }}
                                                        </div>
                                                    </div>
                                                </a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                    <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                        @foreach(auth()->user()->notifications as $notification)
                                            @if($notification->data['type'] == 'log')
                                                <a href="#" class="kt-notification__item @if($notification->read_at != null) kt-notification__item--read @endif" data-id="{{ $notification->id }}">
                                                    <div class="kt-notification__item-icon">
                                                        <i class="{{ $notification->data['icon'] }} kt-font-{{ $notification->data['icon_color'] }}"></i>
                                                    </div>
                                                    <div class="kt-notification__item-details">
                                                        <div class="kt-notification__item-title">
                                                            {{ $notification->data['title'] }}
                                                        </div>
                                                        <div class="kt-notification__item-time">
                                                            {{ $notification->updated_at->diffForHumans() }}
                                                        </div>
                                                    </div>
                                                </a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end: Notifications -->

                <!--begin: User bar -->
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px" aria-expanded="false">
                        <span class="kt-header__topbar-welcome">Bonjour,</span>
                        <span class="kt-header__topbar-username">{{ auth()->user()->name }}</span>
                        @if(\Thomaswelton\LaravelGravatar\Facades\Gravatar::exists(auth()->user()->email))
                            <img class="kt-hidden-" alt="Pic" src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src(auth()->user()->email) }}">
                        @else
                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden-">{{ \App\Helpers\Generator::firsLetter(auth()->user()->name) }}</span>
                        @endif
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">
                            <div class="kt-user-card__avatar">
                                @if(\Thomaswelton\LaravelGravatar\Facades\Gravatar::exists(auth()->user()->email))
                                <img class="kt-hidden-" alt="Pic" src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src(auth()->user()->email) }}" />
                                @else
                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ \App\Helpers\Generator::firsLetter(auth()->user()->name) }}</span>
                                @endif
                            </div>
                            <div class="kt-user-card__name">
                                {{ auth()->user()->name }}
                            </div>
                            <div class="kt-user-card__badge">
                                <!--<span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>-->
                            </div>
                        </div>

                        <!--end: Head -->

                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <a href="" class="kt-notification__item" data-toggle="kt-tooltip" title="Bientôt Disponible">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-calendar-3 kt-font-success"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Mon compte
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Configuration de mon profil & autres
                                    </div>
                                </div>
                            </a>
                            <a href="" class="kt-notification__item" data-toggle="kt-tooltip" title="Bientôt Disponible">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-mail kt-font-warning"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Ma boite de reception
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Conversée avec les autres commerciaux
                                    </div>
                                </div>
                            </a>
                            <a href="" class="kt-notification__item" data-toggle="kt-tooltip" title="Bientôt Disponible">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-calendar kt-font-danger"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Mon agenda
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Voir vos rendez-vous, etc...
                                    </div>
                                </div>
                            </a>
                            <a href="" class="kt-notification__item" data-toggle="kt-tooltip" title="Bientôt Disponible">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-hourglass kt-font-brand"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Mes Taches
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Gérez l'ensembles de vos tâches
                                    </div>
                                </div>
                            </a>
                            <div class="kt-notification__custom kt-space-between">
                                <a href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold">Déconnexion</a>
                            </div>
                        </div>

                        <!--end: Navigation -->
                    </div>
                </div>

                <!--end: User bar -->
            </div>

            <!-- end:: Header Topbar -->
        </div>
    </div>
    <div class="kt-header__bottom">
        <div class="kt-container ">

            <!-- begin: Header Menu -->
            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                    <ul class="kt-menu__nav ">
                        <li class="kt-menu__item  kt-menu__item--open {{ \App\Helpers\Generator::currentRoute(route('home')) }}">
                            <a href="{{ route('home') }}" class="kt-menu__link">
                                <span class="kt-menu__link-text">Tableau de Bord</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                        <li class="kt-menu__item  kt-menu__item--open {{ \App\Helpers\Generator::currentRoute(route('Prospect.index')) }}">
                            <a href="{{ route('Prospect.index') }}" class="kt-menu__link">
                                <span class="kt-menu__link-text">Mes Prospect</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="kt-header-toolbar">
                    <a href="https://demo.francebilletreduc.shop" class="btn btn-primary">Site francebilletreduc.shop</a>
                    <div class="kt-quick-search kt-quick-search--inline kt-quick-search--result-compact" id="kt_quick_search_inline">
                        <form method="get" class="kt-quick-search__form">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                                <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close" style="display: none;"></i></span></div>
                            </div>
                        </form>
                        <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="0px,10px"></div>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                            <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end: Header Menu -->
        </div>
    </div>
</div>
