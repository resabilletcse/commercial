@extends("email.layout.email")

@section("content")
    <p class="titleGuard">Cher {{ $user->name }},</p>
    <p>Voici le code de vérification dont vous avez besoin pour vous connecter au compte {{ $user->email }}</p>
    <p class="code">{{ $code }}</p>

    <div class="grid">
        <p>Cette email vous à été envoyé car une tentative de connexion depuis un ordinateur identifié par l’adresse {{ $ipAdress }} (FR) à été enregistrée. La tentative de connexion renseignait correctement vos identifiants.</p>
        <p>Le code de vérification est nécessaire pour vous connecter. <strong>Personne ne peut accéder à votre compte sans avoir accès à cette email.</strong></p>
        <p><strong>Si vous n’êtes pas à l’origine de cette demande</strong>, veuillez changer votre mot de passe par l’intermédiaire de votre espace “Rubrique Profil”.</p>
        <p style="font-size: 11px; color: #818181;">Si vous n’avez plus accès à votre compte, utilisez ce <a href="{{ route('password.request') }}">lien de récupération de compte.</a> Nous vous aiderons à soit récupérer votre compte soit le verrouiller.</p>
    </div>
@endsection
