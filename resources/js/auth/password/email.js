import * as $ from "jquery";

class EmailPassword {
    constructor () {
        this.form = $("#login_request_password")
    }

    verifyMail() {
        let field = document.querySelector('#emailField')
        let feedValid = document.querySelector('.valid-feedback')
        let feedInvalid = document.querySelector('.invalid-feedback')
        let btn = $("#login_request_password_submit")

        btn.attr('disabled', true)

        field.addEventListener('blur', function(e) {
            e.preventDefault()
            $.ajax({
                url: '/api/verifyMail',
                method: "GET",
                data: {email: field.value},
                statusCode: {
                    200: function (data) {
                        field.classList.add('is-valid')
                        field.classList.remove('is-invalid')
                        feedValid.innerHTML = `L'adresse Mail existe en base de donnée`
                        feedInvalid.innerHTML = null
                        btn.removeAttr('disabled')
                    },
                    422: function (data) {
                        field.classList.add('is-invalid')
                        field.classList.remove('is-valid')
                        feedValid.innerHTML = null;
                        feedInvalid.innerHTML = "L'adresse Mail n'existe pas dans la base de donnée"
                        btn.attr('disabled', true)
                    },
                    500: function (jqxhr) {
                        toastr.error("Requete echouer", "Erreur 500")
                        btn.attr('disabled', true)
                    }
                }
            })

        })
    }

    passwordRequest() {
        this.form.on('submit', function (e) {
            e.preventDefault()
            let btn = $("#login_request_password_submit")

            KTApp.progress(btn)

            $.ajax({
                url: $(this).attr('action'),
                method: "POST",
                data: $(this).serializeArray(),
                statusCode: {
                    200: function (data) {
                        KTApp.unprogress(btn)
                        $(".alert").html(`<div class="alert alert-outline-success fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon2-check-mark"></i></div>
                            <div class="alert-text">Un email vous à été envoyer à l'adresse <strong>${data.data.email}</strong></div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>`)
                    },
                    422: function (data) {
                        KTApp.unprogress(btn)
                        $(".alert").html(`<div class="alert alert-outline-danger fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon2-time"></i></div>
                            <div class="alert-text">Une erreur de champs empèche l'envoie du formulaire</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>`)
                    },
                    500: function (jqxhr) {
                        KTApp.unprogress(btn)
                        $(".alert").html(`<div class="alert alert-outline-danger fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon2-time"></i></div>
                            <div class="alert-text">Une erreur de champs empèche l'envoie du formulaire: <i>${jqxhr.responseText}</i></div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>`)
                    }
                }
            })
        })
    }

}

const init = function() {
    let password = new EmailPassword()
    password.verifyMail()
    password.passwordRequest()
}

init();
