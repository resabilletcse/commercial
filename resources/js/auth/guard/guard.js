import * as $ from "jquery";
import Core from '../../core'

class Guard {
    constructor () {
        this.form = $("#guard_form")
    }

    postGuard() {
        this.form.on('submit', function (e) {
            e.preventDefault()
            let core = new Core()
            let btn = $("#btnPostGuard");
            let form = $("#guard_form")

            KTApp.progress(btn)

            $.ajax({
                url: form.attr('action'),
                method: "POST",
                data: form.serializeArray(),
                statusCode: {
                    200: function (data) {
                        KTApp.unprogress(btn)
                        window.location.href=data.linker
                    },
                    422: function (data) {
                        KTApp.unprogress(btn)
                        core.errorMsg(form, 'warning', data.responseJSON.error)
                    },
                    500: function (data) {
                        KTApp.unprogress(btn)
                        core.errorMsg(form, 'danger', data.responseJSON.error)
                    }
                }
            })
        })
    }
}

const init = function() {
    let guard = new Guard()
    guard.postGuard()
}

init();
