import * as $ from "jquery";

function previousRdv() {
    let div = document.querySelector('#rdvPrevious')

    $.ajax({
        url: '/prospect/api/previousRdv',
        statusCode: {
            200: function (data) {
                div.innerHTML = data.data
            },
            500: function (jqxhr) {
                console.log(jqxhr)
            }
        }
    })
}

function loadRdv() {
    let div = document.querySelector('#loadRdv')

    $.ajax({
        url: '/prospect/api/loadRdv',
        statusCode: {
            200: function (data) {
                div.innerHTML = data.data.content
            },
            500: function (jqxhr) {
                console.log(jqxhr)
            }
        }
    })
}

function registerMedia() {
    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;
    navigator.getUserMedia(
        {
            audio: true
        },
        function (e) {
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            let context = new AudioContext();

            // creates an audio node from the microphone incoming stream
            let mediaStream = context.createMediaStreamSource(e);
            // https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/createScriptProcessor
            let bufferSize = 2048;
            let numberOfInputChannels = 2;
            let numberOfOutputChannels = 2;
            if (context.createScriptProcessor) {
                let recorder = context.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            } else {
                let recorder = context.createJavaScriptNode(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            }
            recorder.onaudioprocess = function (e) {
                console.log("on audio progress");
            }
            // we connect the recorder with the input stream
            mediaStream.connect(recorder);
            recorder.connect(context.destination);
        },
        function (e) {
            // error
            console.error(e);
        });
}

previousRdv()
loadRdv()
registerMedia()
