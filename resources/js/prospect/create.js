import * as $ from "jquery";
import Inputmask from "inputmask";
const Push = require('push.js')

class ProspectCreate{
    constructor() {

    }

    formSubmit() {
        let form = $("#formNewProspect");

        document.querySelector('#formNewProspect').addEventListener('submit', function (e) {
            e.preventDefault()
            let btn = $("#btnSubmitForm")
            let data = form.serializeArray()
            let url = form.attr('action')

            KTApp.progress(btn)

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                statusCode: {
                    200: function (data) {
                        KTApp.unprogress(btn)
                        toastr.success(`Le prospect <strong>${data.name}</strong> à été créer avec succès!`, "Création d'un prospect")
                        Push.create({
                            body: `Le prospect <strong>${data.name}</strong> à été créer avec succès!`,
                            icon: '/storage/icons/correct.png',

                        })
                        document.querySelector('#formNewProspect').reset()
                        console.log(data)
                    },
                    422: function (data) {
                        KTApp.unprogress(btn)
                        Array.from(data.responseJSON.errors).forEach((item) => {
                            toastr.error(item, "Champs Invalide")
                        })
                        console.log(data.responseJSON.errors)
                    },
                    500: function (jqxhr) {
                        KTApp.unprogress(btn)
                        console.log(jqxhr)
                    }
                }
            })
        })
    }
}

const Init = function () {
    let prospect = new ProspectCreate()
    prospect.formSubmit()

    Inputmask({mask: "99 99 99 99 99"}).mask(document.querySelector('#tel_fixe'))
    Inputmask({mask: "99 99 99 99 99"}).mask(document.querySelector('#tel_portable'))
    Inputmask({mask: "99999"}).mask(document.querySelector('#code_postal'))
}

Init()
