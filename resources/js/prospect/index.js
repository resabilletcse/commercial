import * as $ from "jquery";

class ProspectIndex {
    constructor() {
        this.list = document.querySelector('#list')
    }

    init() {
        KTApp.block(this.list)

        $.get('/prospect/api/list')
            .done((data) => {
                this.list.innerHTML = data.data
            })

    }

    upCount() {
        let element = document.querySelector('#kt_subheader_total')

        KTApp.progress(element)

        $.get('/prospect/api/count')
            .done((data) => {
                element.innerHTML = data.data
            })
    }

    changeStatus() {
        let elements = document.querySelectorAll('.kt-nav__link')

        Array.from(elements).forEach((item) => {
            item.addEventListener('click', function (e) {
                e.preventDefault();
                KTApp.progress(this.list)

                $.post('/prospect/api/list', {
                    status: item.dataset.status
                })
                    .done((data) => {
                        $("#list").html(data.data)
                    })
            })
        })
    }
}

const Init = function () {
    let prospect = new ProspectIndex()
    prospect.init()
    prospect.upCount()
    prospect.changeStatus()
}

Init()
