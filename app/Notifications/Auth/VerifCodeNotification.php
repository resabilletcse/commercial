<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifCodeNotification extends Notification
{
    use Queueable;
    /**
     * @var
     */
    private $user;
    /**
     * @var
     */
    private $code;
    /**
     * @var
     */
    private $ipAdress;

    /**
     * Create a new notification instance.
     *
     * @param $user
     * @param $code
     * @param $ipAdress
     */
    public function __construct($user, $code, $ipAdress)
    {
        //
        $this->user = $user;
        $this->code = $code;
        $this->ipAdress = $ipAdress;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(env("APP_NAME").": Accès depuis un nouvel ordinateur")
                    ->view('email.auth.guardCode', [
                        "user"  => $this->user,
                        "code" => $this->code,
                        "ipAdress" => $this->ipAdress
                    ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fa fa-key",
            "icon_color" => "info",
            "type"  => "alert",
            "title" => "Veuillez vous identifier avec le code reçu par mail",
            "text" => "Une connexion frauduleuse à été detecter par le système, veuillez vérifier vos mails et entrez le code d'autorisation",
            "date"  => now(),
            "link"  => route('Account.locked')
        ];
    }
}
