<?php

namespace App\Http\Middleware;

use Closure;

class checkCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->ip() == auth()->user()->lastIp)
        {
            return $next($request);
        }else{
            return redirect()->route('logout');
        }
    }
}
