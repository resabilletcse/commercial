<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware(["checkcode"]);
    }

    public function index()
    {
        return view("account.profil.index");
    }
}
