<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Repository\Inbox\InboxRepository;
use App\Repository\User\UserRepository;
use Illuminate\Http\Request;

class InboxApiController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var InboxRepository
     */
    private $inboxRepository;

    /**
     * InboxApiController constructor.
     * @param UserRepository $userRepository
     * @param InboxRepository $inboxRepository
     */
    public function __construct(UserRepository $userRepository, InboxRepository $inboxRepository)
    {
        $this->userRepository = $userRepository;
        $this->inboxRepository = $inboxRepository;
    }

    public function getList($type_box)
    {
        $type = $this->typeBox($type_box);
        $datas = $this->inboxRepository->listFormBox($type);
        ob_start();
        ?>
        <?php foreach ($datas as $data): ?>
        <div class="kt-inbox__item <?php if($data->message->read_at == null): ?> kt-inbox__item--unread <?php endif; ?>" data-id="<?= $data->id; ?>" data-type="<?= $type_box; ?>">
            <div class="kt-inbox__info">
                <div class="kt-inbox__actions">
                    <label class="kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand">
                        <input type="checkbox">
                        <span></span>
                    </label>
                </div>
                <div class="kt-inbox__sender" data-toggle="view">
															<span class="kt-media kt-media--sm kt-media--danger" style="background-image: url('assets/media/users/100_13.jpg')">
																<span></span>
															</span>
                    <a href="#" class="kt-inbox__author"><?= $data->userFrom->name; ?></a>
                </div>
            </div>
            <div class="kt-inbox__details" data-toggle="view">
                <div class="kt-inbox__message">
                    <span class="kt-inbox__subject"><?= $data->subject; ?> - </span>
                    <span class="kt-inbox__summary">Thank you for ordering UFC 240 Holloway vs Edgar Alternate camera angles...</span>
                </div>
            </div>
            <div class="kt-inbox__datetime" data-toggle="view">
                <?= $data->updated_at->diffForHumans(); ?>
            </div>
        </div>
    <?php endforeach; ?>
        <?php
        $content = ob_get_clean();

        return $this->sendResponse($content, "OK");
    }

    private function getInitialState($number)
    {
        switch ($number) {
            case 0:
                return "success";
            case 1:
                return "warning";
            case 2:
                return "danger";
            case 3:
                return "info";
            case 4:
                return "primary";
        }
    }

    private function typeBox($type)
    {
        switch ($type) {
            case 'inbox':
                return 0;

            case 'sent':
                return 1;

            case 'marked':
                return 2;

            case 'trash':
                return 3;
        }
    }
}
