<?php

namespace App\Http\Controllers;

use App\Mail\Auth\GuardCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TestController extends Controller
{
    public function code()
    {
        dd(auth()->user()->notifications);
    }

    public function mail()
    {
        $user = User::find(1);
        $code = Str::upper(Str::random(4));
        return new GuardCode($user, $code, \request()->getClientIp());
    }
}
