<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    public function login()
    {
        if (Auth::attempt([
            "email" => \request('email'),
            "password" => \request('password')
        ])) {
            $user = Auth::user();
            $success['token'] = $user->createToken(env("APP_NAME"))->accessToken;
            return $this->sendResponse($success, "Connexion Réussi");
        } else {
            return $this->sendError("Erreur de Conexion", ["error" => "Unauthorised"], 401);
        }
    }
}
