<?php

namespace App\Http\Controllers\Api\Prospect;

use App\Helpers\Prospect;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Repository\Prospect\ProspectRepository;
use Illuminate\Http\Request;

class ProspectController extends BaseController
{
    /**
     * @var ProspectRepository
     */
    private $prospectRepository;

    /**
     * ProspectController constructor.
     * @param ProspectRepository $prospectRepository
     */
    public function __construct(ProspectRepository $prospectRepository)
    {
        $this->prospectRepository = $prospectRepository;
    }

    public function changeStatus($prospect_id, $status)
    {
        try {
            $data = $this->prospectRepository->changeStatus($prospect_id, $status);

            return $this->sendResponse($data->toArray(), "Mise à jours de Status");
        }catch (\Exception $exception) {
            return $this->sendError("Erreur", $exception->getMessage(), 500);
        }
    }
}
