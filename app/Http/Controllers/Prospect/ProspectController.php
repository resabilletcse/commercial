<?php

namespace App\Http\Controllers\Prospect;

use App\Http\Controllers\Controller;
use App\Repository\Prospect\ProspectRepository;
use Illuminate\Http\Request;
use JeroenDesloovere\VCard\VCard;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Validator;

class ProspectController extends Controller
{
    /**
     * @var ProspectRepository
     */
    private $prospectRepository;

    /**
     * ProspectController constructor.
     * @param ProspectRepository $prospectRepository
     */
    public function __construct(ProspectRepository $prospectRepository)
    {
        $this->prospectRepository = $prospectRepository;
    }

    public function index()
    {
        return view("prospect.index");
    }

    public function create()
    {
        return view("prospect.create");
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "societe" => "required",
            "name" => "required",
            "adresse" => "required",
            "code_postal" => "required|min:5|max:5",
            "ville" => "required",
            "email" => "required|email|unique:prospects",
            "source" => "required",
            "status" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()->all()], 422);
        }else{
            try {
                $this->prospectRepository->create(
                    $request->societe,
                    $request->name,
                    $request->adresse,
                    $request->code_postal,
                    $request->ville,
                    $request->email,
                    $request->tel_fixe,
                    $request->tel_portable,
                    $request->source,
                    $request->status
                );
                return response()->json(["message" => "Le prospect à été créer avec succès", "name" => $request->name], 200);
            }catch (\Exception $exception) {
                return response()->json(["errors" => $exception->getMessage()], 500);
            }
        }
    }

    public function show($prospect_id)
    {
        return view("prospect.show", [
            "prospect" => $this->prospectRepository->get($prospect_id)
        ]);
    }

    public function vcard($prospect_id)
    {
        $prospect = $this->prospectRepository->get($prospect_id);

        $vcard = new VCard();

        $vcard->addName($prospect->name);

        $vcard->addCompany($prospect->societe);
        $vcard->addEmail($prospect->email);
        $vcard->addAddress(null, null, $prospect->adresse, $prospect->ville, null, $prospect->code_postal, 'France');
        if(!empty($prospect->tel_fixe)){$vcard->addPhoneNumber($prospect->tel_fixe, 'FIXE');}
        if(!empty($prospect->tel_portable)){$vcard->addPhoneNumber($prospect->tel_portable, 'PORTABLE');}
        if(Gravatar::exists($prospect->email)){$vcard->addPhoto(Gravatar::src($prospect->email));}

        return $vcard->download();
    }
}
