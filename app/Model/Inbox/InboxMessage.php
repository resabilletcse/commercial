<?php

namespace App\Model\Inbox;

use Illuminate\Database\Eloquent\Model;

class InboxMessage extends Model
{
    protected $guarded = [];
    protected $dates = ["created_at", "updated_at", "read_at"];

    public function inbox()
    {
        return $this->belongsTo(Inbox::class, 'inbox_id');
    }
}
