<?php

namespace App\Model\Agenda;

use App\Model\Prospect\Prospect;
use App\User;
use Illuminate\Database\Eloquent\Model;

class AgendaEvent extends Model
{
    protected $guarded = [];
    protected $dates = ["created_at", "updated_at", "start", "end"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function prospect()
    {
        return $this->belongsTo(Prospect::class, 'prospect_id');
    }
}
