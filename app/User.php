<?php

namespace App;

use App\Model\Agenda\AgendaEvent;
use App\Model\Inbox\Inbox;
use App\Model\Prospect\Prospect;
use App\Model\Task\Task;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'lastIp', 'verifCode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    public function inboxes()
    {
        return $this->hasMany(Inbox::class);
    }

    public function agendas()
    {
        return $this->hasMany(AgendaEvent::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function prospects()
    {
        return $this->hasMany(Prospect::class);
    }
}
