<?php
namespace App\Repository\Inbox;

use App\Model\Inbox\InboxMessage;

class InboxMessageRepository
{
    /**
     * @var InboxMessage
     */
    private $inboxMessage;

    /**
     * InboxMessageRepository constructor.
     * @param InboxMessage $inboxMessage
     */

    public function __construct(InboxMessage $inboxMessage)
    {
        $this->inboxMessage = $inboxMessage;
    }

}

