<?php
namespace App\Repository\User;

use App\User;

class UserRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function countEmail($email) {
        return $this->user->newQuery()
            ->where('email', $email)
            ->count();
    }

    public function list()
    {
        return $this->user->newQuery()
            ->get();
    }

}

