<?php
namespace App\Repository\Task;

use App\Model\Task\Task;

class TaskRepository
{
    /**
     * @var Task
     */
    private $task;

    /**
     * TaskRepository constructor.
     * @param Task $task
     */

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

}

