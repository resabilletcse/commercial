<?php

use Illuminate\Database\Seeder;

class ProspectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \Faker\Generator $faker
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        $users = \App\User::all();

        foreach ($users as $user) {
            for($i=0; $i < 10; $i++) {
                \App\Model\Prospect\Prospect::create([
                    "user_id" => $user->id,
                    "societe" => $faker->company,
                    "name" => $faker->name,
                    "adresse" => $faker->streetAddress,
                    "code_postal" => $faker->postcode,
                    "ville" => $faker->city,
                    "email" => $faker->email,
                    "tel_fixe" => $faker->phoneNumber,
                    "tel_portable" => $faker->phoneNumber,
                    "description" => $faker->text(),
                    "source" => rand(0,2),
                    "status" => rand(0,3)
                ]);
            }
        }
    }
}
