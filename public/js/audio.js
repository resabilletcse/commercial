/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/audio.js":
/*!*******************************!*\
  !*** ./resources/js/audio.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

var audio = document.querySelector('audio');
var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
var recorder; // globally accessible

var microphone;
var btnStartRecording = document.getElementById('btn-start-recording');
var btnStopRecording = document.getElementById('btn-stop-recording');

function capture(callback) {
  if (microphone) {
    callback(microphone);
    return;
  }

  if (typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
    toastr.error("Ce navigateur ne support pas l'enregistrement audio");

    if (!!navigator.getUserMedia) {
      toastr.warning("Ce navigateur semble prendre en charge l'API getUserMedia obsolète.");
    }
  }

  navigator.mediaDevices.getUserMedia({
    audio: isEdge ? true : {
      echoCancellation: false
    }
  }).then(function (mic) {
    callback(mic);
  })["catch"](function (err) {
    toastr.error("Impossible de capturer votre microphone. Veuillez vérifier les journaux de la console.");
    console.log(err);
  });
}

function replaceAudio(src) {
  var newAudio = document.createElement('audio');
  newAudio.controls = true;
  newAudio.autoplay = true;

  if (src) {
    newAudio.src = src;
  }

  var parentNode = audio.parentNode;
  parentNode.innerHTML = '';
  parentNode.appendChild(newAudio);
  audio = newAudio;
}

function stopRecordingCallback() {
  replaceAudio(URL.createObjectURL(recorder.getBlob()));
  setTimeout(function () {
    if (!audio.paused) return;
    setTimeout(function () {
      if (!audio.paused) return;
      audio.play();
    }, 1000);
    audio.play();
  }, 300);
  audio.play();
}

btnStartRecording.onclick = function () {
  this.disabled = true;
  this.style.border = '';
  this.style.fontSize = '';

  if (!microphone) {
    capture(function (mic) {
      microphone = mic;

      if (isSafari) {
        replaceAudio();
        audio.muted = true;
        audio.srcObject = microphone;
        btnStartRecording.disabled = false;
        btnStartRecording.style.border = '1px solid red';
        btnStartRecording.style.fontSize = '150%';
        toastr.error('Please click startRecording button again. First time we tried to access your microphone. Now we will record it.');
        return;
      }

      click(btnStartRecording);
    });
    return;
  }

  replaceAudio();
  audio.muted = true;
  audio.srcObject = microphone;
  var options = {
    type: 'audio',
    numberOfAudioChannels: isEdge ? 1 : 2,
    checkForInactiveTracks: true,
    bufferSize: 16384
  };

  if (isSafari || isEdge) {
    options.recorderType = StereoAudioRecorder;
  }

  if (navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
    options.sampleRate = 48000; // or 44100 or remove this line for default
  }

  if (isSafari) {
    options.sampleRate = 44100;
    options.bufferSize = 4096;
    options.numberOfAudioChannels = 2;
  }

  if (recorder) {
    recorder.destroy();
    recorder = null;
  }

  recorder = new RecordRTC(microphone, options);
  recorder.startRecording();
  btnStopRecording.disabled = false;
  btnDownloadRecording.disabled = true;
};

btnStopRecording.onclick = function () {
  this.disabled = true;
  recorder.stopRecording(stopRecordingCallback);
};

/***/ }),

/***/ 3:
/*!*************************************!*\
  !*** multi ./resources/js/audio.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\LOGICIEL\laragon\www\commercial.resabilletcse\resources\js\audio.js */"./resources/js/audio.js");


/***/ })

/******/ });