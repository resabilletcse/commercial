<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/verifyMail', 'Api\CoreController@verifyMail');

Route::post('login', 'Api\Auth\AuthController@login');
Route::post('register', 'Api\Auth\AuthController@register');

Route::group(["prefix" => "account", "namespace" => "Api\Account"], function (){
    Route::get('notification/{notification_id}/read', "AccountController@readNotification");

    Route::group(["prefix" => "inbox", "namespace" => "Inbox"], function (){
        Route::get('getList/{type_box}', 'InboxController@getList')->middleware(["auth:api"]);
    });
});

Route::group(["prefix" => "prospect", "namespace" => "Api\Prospect"], function (){
    Route::get('{prospect_id}/changeStatus/{status}', 'ProspectController@changeStatus');
});
